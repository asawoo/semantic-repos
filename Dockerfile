FROM stain/jena-fuseki:2.6.0

ADD ontologies ontologies
CMD ["/jena-fuseki/fuseki-server", "--config=/jena-fuseki/ontologies/fuseki-config.ttl"]
