# Semantic repositories for the ASAWoO platform

This is based on a docker image of Fuseki, the sparql server that is part of Jena.


Build the image and run a container :

    ./build_and_run.sh

Only build the image :

    docker build -t asawoo/semantic-repos .

Only run a container :

    docker run --name semantic-repos -p 3030:3030 -v data:/fuseki -e "ADMIN_PASSWORD=admin123asawoo" --rm asawoo/semantic-repos

Check that it is alive using the [web view](http://localhost:3030), use admin/admin123asawoo to connect to the manager.

## Examples

Use the [sparql query editor](http://localhost:3030/dataset.html?tab=query&ds=/asawoo) to run most of the following queries, some of them use curl.

### Explore base vocabularies

List the available named graphs:

    SELECT ?g
    WHERE {
      GRAPH ?g { }
    }

Get all the predicates from the ASAWoO core vocabulary:

    SELECT ?subject ?predicate ?object
    FROM <http://liris.cnrs.fr/asawoo/vocab>
    WHERE {
      ?subject ?predicate ?object
    }

Get all the predicates from the ASAWoO context vocabulary:

    SELECT ?subject ?predicate ?object
    FROM <http://liris.cnrs.fr/asawoo/vocab/context>
    WHERE {
      ?subject ?predicate ?object
    }

Get all the predicates from the SSN vocabulary:

    SELECT ?subject ?predicate ?object
    FROM <http://www.w3.org/ns/ssn/>
    WHERE {
      ?subject ?predicate ?object
    }

### Asawoo functionalities, capabilities and devices

Get all known functionality classes:

    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX asawoo: <http://liris.cnrs.fr/asawoo/vocab#>

    SELECT ?functionalityClass
    FROM <http://liris.cnrs.fr/asawoo/functionalities>
    WHERE {
      ?functionalityClass rdfs:subClassOf asawoo:Functionality
    }

Get all known capability classes and the functionality classes they implement:

    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX asawoo: <http://liris.cnrs.fr/asawoo/vocab#>

    SELECT ?capabilityClass ?capabilityClassLabel ?functionalityClass
    FROM <http://liris.cnrs.fr/asawoo/capabilities>
    WHERE {
      ?capabilityClass rdfs:subClassOf asawoo:Capability .
      ?capabilityClass rdfs:label ?capabilityClassLabel .
      ?functionalityClass asawoo:isImplementedBy ?capabilityClass .
    }

Get the properties associated to a specific capability class:

    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX asawoo: <http://liris.cnrs.fr/asawoo/vocab#>
    PREFIX asawoo-c: <http://liris.cnrs.fr/asawoo/capabilities#>

    SELECT ?property ?label ?range
    FROM <http://liris.cnrs.fr/asawoo/capabilities>
    WHERE {
      ?property rdfs:domain asawoo-c:HTTPCamera .
      ?property rdfs:label ?label .
      ?property rdfs:range ?range .
    }

Get functionalities compositions:

    PREFIX asawoo: <http://liris.cnrs.fr/asawoo/vocab#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    SELECT ?f1 ?f2
    FROM <http://liris.cnrs.fr/asawoo/functionalities>
    WHERE {
      ?f1 asawoo:isComposedOf [ owl:unionOf/rdf:rest*/rdf:first ?f2 ] .
    }

Get adaptation purposes and their respective adaptation candidate types:

    PREFIX asawoo-ctx: <http://liris.cnrs.fr/asawoo/vocab/context#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

    SELECT ?adaptPurpose ?adaptCandidate
    FROM <http://liris.cnrs.fr/asawoo/vocab/context>
    WHERE {
    	?adaptPurpose rdf:type asawoo-ctx:AdaptationPurpose .
    	?adaptPurpose asawoo-ctx:purposePredicate ?purposePred .
    	?purposePred rdfs:range ?adaptCandidate .
    }

Insert a gopigo device with all its capabilities:

    curl -XPOST http://localhost:3030/asawoo/update -d 'update=
        PREFIX asawoo-d: <http://liris.cnrs.fr/asawoo/devices#>
        PREFIX asawoo-c: <http://liris.cnrs.fr/asawoo/capabilities#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>              
        PREFIX ssn: <http://www.w3.org/ns/ssn/>

        INSERT DATA {
          GRAPH <http://liris.cnrs.fr/asawoo/devices> {
          	asawoo-d:Gopigo_1 rdf:type ssn:System .
            asawoo-d:Gopigo_1 rdfs:label "My Gopigo" .
            asawoo-d:Gopigo_1_Motion rdf:type asawoo-c:GopigoMotion .
            asawoo-d:Gopigo_1_Camera rdf:type asawoo-c:GopigoCamera .
            asawoo-d:Gopigo_1_Led rdf:type asawoo-c:GopigoLed .
            asawoo-d:Gopigo_1 ssn:implements asawoo-d:Gopigo_1_Motion .
            asawoo-d:Gopigo_1 ssn:implements asawoo-d:Gopigo_1_Camera .
            asawoo-d:Gopigo_1 ssn:implements asawoo-d:Gopigo_1_Led .
          }
        }
    '

Insert a remote device with a HTTP video stream:

    curl -XPOST http://localhost:3030/asawoo/update -d 'update=
        PREFIX asawoo-d: <http://liris.cnrs.fr/asawoo/devices#>
        PREFIX asawoo-c: <http://liris.cnrs.fr/asawoo/capabilities#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>         
        PREFIX ssn: <http://www.w3.org/ns/ssn/>

        INSERT DATA {
          GRAPH <http://liris.cnrs.fr/asawoo/devices> {
            asawoo-d:Remote_1 rdf:type ssn:System .
            asawoo-d:Remote_1 rdfs:label "My security camera" .
            asawoo-d:Remote_1_Camera rdf:type asawoo-c:HTTPCamera .
            asawoo-d:Remote_1_Camera asawoo-c:httpCameraUrl "http://localhost:8083" .
            asawoo-d:Remote_1 ssn:implements asawoo-d:Remote_1_Camera .
          }
        }
    '

Get all devices, their capabilities and their properties:

    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX ssn: <http://www.w3.org/ns/ssn/>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX asawoo-c: <http://liris.cnrs.fr/asawoo/capabilities#>
    PREFIX asawoo-d: <http://liris.cnrs.fr/asawoo/devices#>
    PREFIX asawoo-f: <http://liris.cnrs.fr/asawoo/functionalities#>
    PREFIX asawoo: <http://liris.cnrs.fr/asawoo/vocab#>

    SELECT ?device ?deviceLabel ?capability ?property ?propertyValue ?capabilityClass ?functionalityClass
    FROM <http://liris.cnrs.fr/asawoo/devices>
    FROM <http://liris.cnrs.fr/asawoo/capabilities>
    WHERE {
      ?device rdf:type ssn:System .
      OPTIONAL { ?device rdfs:label ?deviceLabel }
      OPTIONAL {
        ?device ssn:implements ?capability .
        ?capability rdf:type ?capabilityClass .
        ?functionalityClass asawoo:isImplementedBy ?capabilityClass
        OPTIONAL {
          ?capability ?property ?propertyValue .
          ?property rdf:type owl:ObjectProperty
        }
      }
    }
