#!/bin/bash

docker build -t asawoo/semantic-repos .
docker rm -f semantic-repos
docker run -d --name semantic-repos -p 3030:3030 -v data:/fuseki -e "ADMIN_PASSWORD=admin123asawoo" asawoo/semantic-repos || exit 1

echo "Container 'semantic-repos' is running."
echo "You can try 'docker logs semantic-repos'"
echo "You can open http://localhost:3030 with admin/admin123asawoo credentials"
